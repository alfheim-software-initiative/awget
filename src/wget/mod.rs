extern crate reqwest;

use std::fs::File;
use std::io::copy;
use std::str;

pub fn w(s: &str, d: &str) -> Result<(), Box<dyn std::error::Error>> {
    let target: &str = s;
    let mut res = reqwest::get(target)?;
    let mut dest = File::create(d)?;
    copy(&mut res, &mut dest)?;

    Ok(())
}
